# Global Stub

The intention of this project was to come up with an easily configurable centralized Stub or Mock service system for seamless integration. The idea was a product of my past experience with integration projects. 
Integration projects always has dependencies with different systems and vendors where in the Unit testing of the project will required lots of mocking either using SOAP UI or some other tools. I hope this project would be 
a better solution since it is more centralized and easy to configure new stubs.

## Getting Started

You can download/clone the project in to your eclipse workspace directly and use maven to build. Do an mvn eclipse:eclipse to generate the eclipse project settings.

### Prerequisites

1. Maven 3
2. Mongo DB
3. Tested in Apache Tomcat 8

### Installing

Start Mongodb server and create a new database (say - devportal).

Modify the properties mentioned in resources/application.properties if you intent to change the context path, server port or mongodb url.

Use maven or eclipse to build the war file and deploy it in your server.

After installation, navigation to the application Admin->New Stub screen and configure your new Stub

* Stub Name - Any name that you would like to call the stub 
* Soap Action - The soap action of the operation for which you intend to create the stub. Note that the stub works based on the soap action that you provide here 
* Stub Url - Needed only if you intend to have an additional url for your stub 
* Stub Type - Synchronous or Asynchronous 
* EnableLogging - To log the requests coming to the stub so that the requests can be verified for the content. 
* Synchronous Response - The response that you expect the stub to respond synchronosly 
* Asynchronous Response - The async response that you expect the stub to respond 
* Callback Url - The call back url for the stub to send async response 
* Correlation Params - You can have multiple correlation params that correlated the request and response asynchronously. The convention used for naming correlation params is explained below. 

For example, consider the below SOAP Request
### SOAP Request
```
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:hel="http://model.exws.com/HelloService/">
   <soapenv:Header/>
   <soapenv:Body>
      <hel:NewOperation>
         <in>test</in>
		 <Data>
		 	<StudentId>123</StudentId>
		 </Data>
      </hel:NewOperation>
   </soapenv:Body>
</soapenv:Envelope>
```
* If <in> is the field that correlates your requuest to a response, then your correlation param is **NewOperation.in**
* If StudentId is the correlation field, then your correlation param is **NewOperation.Data.StudentId**

Save after filling the details. You will get an alert saying the stub has been saved succesfully.
Navigate to dashboard and you can see the new stub there.

### Testing the Stub
Now for testing the stub, Trigger a Soap request from SOAP ui to the global stub url.
For example, http://localhost:9090/GlobalStub/soap and you will see the same SOAP response that you initially configured in Synchronous Response field.
Note: If you have opted additional url in the stub url field during configuration (like /mynewstub), then you will have to trigger requests to http://localhost:9090/GlobalStub/soap/mynewstub

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Faisal Khan** - *Initial work* - [faisalkhan1987](https://bitbucket.org/faisalkhan1987)

## License

This project is licensed under the Apache License - see the [LICENSE.md](LICENSE.md) file for details
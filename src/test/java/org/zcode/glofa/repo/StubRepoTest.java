/**
 * 
 */
package org.zcode.glofa.repo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.zcode.glofa.domain.StubConfig;

/**
 * @author fkhan
 *
 */
@RunWith(SpringRunner.class)
@DataMongoTest
public class StubRepoTest {

	@Autowired
	StubConfigRepo stubConfigRepo;
	
	@Test
	public void testSave(){
		StubConfig stubConfig = StubConfig.builder().soapAction("test").build();
		stubConfigRepo.save(stubConfig);
	}
}

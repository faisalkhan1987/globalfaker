/**
 * 
 */
package org.zcode.glofa.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.Element;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

/**
 * @author fkhan
 *
 */
public class SOAPTest {
	
	private static final String SOAP_REQUEST = "<soapenv:Envelope "
			+ "xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
			+ "xmlns:v003=\"http://xmlns.telenet.be/soa.model/services/network/data/PhoneNumberInventoryDataServiceMessages/v003\" "
			+ "xmlns:v0031=\"http://xmlns.telenet.be/soa.model/types/resource/PhoneNumberAllocation/v003\" "
			+ "xmlns:v002=\"http://xmlns.telenet.be/soa.model/entities/resource/ResourceIdentifiers/v002\" "
			+ "xmlns:v001=\"http://xmlns.telenet.be/soa.model/types/base/TechnicalRequestInfo/v001\" "
			+ "xmlns:v0011=\"http://xmlns.telenet.be/soa.model/entities/base/BaseIdentifiers/v001\" "
			+ "xmlns:v0012=\"http://xmlns.telenet.be/soa.model/entities/base/basetypes/v001\" "
			+ "xmlns:v0013=\"http://xmlns.telenet.be/soa.model/entities/base/MultiTenancy/v001\">"
			+ "   <soapenv:Header/>"
			+ "   <soapenv:Body>"
			+ "      <v003:AllocatePhoneNumberRequest>"
			+ "         <v0031:PhoneNumberAllocation>"
			+ "            <v002:PhoneNumberIdentifiers>"
			+ "               <!--Zero or more repetitions:-->"
			+ "               <v002:PhoneNumberIdentifier>"
			+ "                  <!--You have a CHOICE of the next 2 items at this level-->"
			+ "                  <v002:LocalPhoneNumberId>123123</v002:LocalPhoneNumberId>"
			+ "                  <v002:InternationalPhoneNumberId>1312223</v002:InternationalPhoneNumberId>"
			+ "               </v002:PhoneNumberIdentifier>"
			+ "            </v002:PhoneNumberIdentifiers>"
			+ "            <v001:RequestorIdentifier>" 
			+ "               <!--Optional:-->"
			+ "               <v0011:System>SYSTEM</v0011:System>"
			+ "               <v0012:User>USER</v0012:User>"
			+ "            </v001:RequestorIdentifier>"
			+ "            <v0013:TenantId>1</v0013:TenantId>"
			+ "         </v0031:PhoneNumberAllocation>"
			+ "      </v003:AllocatePhoneNumberRequest>"
			+ "   </soapenv:Body>"
			+ "</soapenv:Envelope>";
	
	
	public static void main(String[] args) throws SOAPException, IOException{
		InputStream in = new ByteArrayInputStream(SOAP_REQUEST.getBytes());
		
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage(new MimeHeaders(), in);
		SOAPBody soapBody = soapMessage.getSOAPBody();
		Iterator itr = soapBody.getChildElements();
		while(itr.hasNext()){
			Element element = (Element) itr.next();
		}
	}

}

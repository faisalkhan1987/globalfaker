/**
 * 
 */
package org.zcode.glofa.utils;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;

/**
 * @author fkhan
 *
 */
public class TextUtilsTest {

	public static void main(String[] args) throws JSONException {

		RegexUrl regexUrl = replaceVariables("http://localhost:9090/GlobalStub/rest/api/v1/orderManagement/productOrder/123/orderItem?locationId=123");
		System.out.println(regexUrl.url);
		for(String var : regexUrl.varList) {
			System.out.println(var);
		}
	}
	
	private static RegexUrl replaceVariables(String stubUrl) {

		RegexUrl regexUrl = new RegexUrl();
		
		StringBuilder strBuilder = new StringBuilder();
		String[] parts = stubUrl.split("[/,=,?]");
		for(String part : parts) {
			if(part.startsWith("$")) {
				regexUrl.varList.add(part);
				regexUrl.nGroups++;
				part = "[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*";
			}
			strBuilder.append(part).append("/");
		}
		regexUrl.url = strBuilder.toString();
		
		return regexUrl;
	}
	
	private static class RegexUrl{
		String url;
		int nGroups=0;
		List<String> varList;
		
		public RegexUrl() {
			nGroups = 0;
			varList = new ArrayList<String>();
		}
	}
}

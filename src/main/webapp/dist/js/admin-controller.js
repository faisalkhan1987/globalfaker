// Code goes here
var app = angular.module('org.zcode.glofa.admin',[]);

app.controller('AdminController', function ($scope, $http){ // , $mdDialog) {
	
	$scope.stub = new Object();
	$scope.stub.inputParams = [];
	$scope.stubUrlPrefix = "";
	
	$scope.saveInputParam = function(){
		$scope.stub.inputParams.push($scope.inputParam);
		$scope.inputParam = '';
	};

	$scope.saveStub = function(){
		$scope.stub.stubUrl = $scope.stubUrlPrefix + $scope.stub.stubUrl;
		$http.post("../admin/save", $scope.stub).
            then(function (response) {
            	alert('Stub has been saved succesfully');
				$scope.stub = '';
            }, function (response) {
            	alert('Unexpected failure occurred');
				console.log(response);
            });
	}
	
	$scope.removeParam = function(index){
		$scope.stub.correlationParams.splice(index, 1);
	}

});

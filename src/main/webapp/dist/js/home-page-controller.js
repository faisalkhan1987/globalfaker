// Code goes here
var app = angular.module('org.zcode.glofa',['ngRoute','org.zcode.glofa.admin','ngPrettyJson']);

app.config(['$routeProvider', function($routeProvider) {
	$routeProvider
      .when('/', {
   		templateUrl : "../pages/dashboard.html"
      }).when("/dashboard", {
   		templateUrl : "../pages/dashboard.html"
      }).when("/stub_logs", {
     		templateUrl : "../pages/stub_logs.html"
      }).when("/admin/new_soap_stub", {
     		templateUrl : "../pages/admin/new_soap_stub.html"
      }).when("/admin/new_rest_stub", {
   		templateUrl : "../pages/admin/new_rest_stub.html"
	});
	  
}]);

app.controller('HomePageController', function ($scope, $http){ // , $mdDialog) {
	
	$http.get('../admin/getAllStubs').
	    then(function (response) {
	        $scope.stubs = response.data;
	    }, function (response) {
	    	alert('Unexpected Failure');
	    });
	
	$scope.searchLogs = function(){
		console.log('Search Logs for '+JSON.stringify($scope.selectedStub.stubName));
		$http.get('../admin/getStubLogsByName?stubName='+$scope.selectedStub.stubName).
	    then(function (response) {
	        $scope.searchResults = response.data;
	        console.log($scope.searchResults);
	    }, function (response) {
	    	alert('Unexpected Failure');
	    });
		
	}
	
});

app.directive('prettyprint', function() {
    return {
        restrict: 'C',
        link: function postLink(scope, element, attrs) {
              element.text(vkbeautify.xml(scope.dom, 4));
        }
    };
});

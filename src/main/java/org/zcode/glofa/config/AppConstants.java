/**
 * 
 */
package org.zcode.glofa.config;

/**
 * @author fkhan
 *
 */
public interface AppConstants {

	public static final String SOAP_URL = "/soap";

	public static final String REST_URL = "/rest";
	
	public static final String SYNC = "SYNC";
	
	public static final String ASYNC = "ASYNC";
	
	public static final String REST = "REST";
	
	public static final String SOAP = "SOAP";
	
	public static final String REGEX_STRING = "[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*";
	
	public static final String EMPTY_STRING = "";
}

package org.zcode.glofa.builder;

import java.util.List;
import java.util.Map;

import javax.xml.soap.SOAPMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zcode.glofa.params.ParamResolver;

/**
 * @author fkhan
 *
 */
@Component("soapResponseBuilder")
public class SoapResponseBuilder implements ContentBuilder<String, SOAPMessage>{

	@Autowired
	ParamResolver<SOAPMessage> soapParamResolver;
	
	@Override
	public String buildContent(SOAPMessage soapMessage, String response, List<String> inputParams) {
		
		if(inputParams != null && inputParams.size() > 0){

			Map<String, String> valueMap = soapParamResolver.resolveParams(inputParams, soapMessage);
			
			for(String param : inputParams){
				response = response.replace(param, valueMap.get(param));
			}
		}
		return response;
	}


}

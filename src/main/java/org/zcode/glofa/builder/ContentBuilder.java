package org.zcode.glofa.builder;

import java.util.List;

public interface ContentBuilder<ContentType, RequestType> {

	/**
	 * @param request
	 * @param stubConfig
	 * @return
	 */
	ContentType buildContent(RequestType request, String response, List<String> inputParams);
}

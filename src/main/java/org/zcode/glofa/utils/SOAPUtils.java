package org.zcode.glofa.utils;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.xml.soap.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

/**
 * @author Faisal_Khan01
 * Sep 26, 2016
 *
 */
@Log4j2
@Component
public class SOAPUtils {

	public static SOAPMessage createSoapMessageFromHttpRequest(HttpServletRequest request) throws IOException, SOAPException {
		MessageFactory messageFactory = MessageFactory.newInstance();
		InputStream inStream = request.getInputStream();
		return messageFactory.createMessage(new MimeHeaders(), inStream);
	}
	
    /**
     * @param request
     * @param url
     * @return
     * @throws SOAPException 
     * @throws UnsupportedOperationException 
     * @throws TransformerException 
     * @throws IOException 
     * @throws Exception
     */
    public SOAPMessage triggerRequest(String request, String url) {
    	log.debug("executeWS : " + url);
        SOAPConnection soapConnection = null;
        SOAPMessage soapResponse = null;
        try {
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
                    .newInstance();
            soapConnection = soapConnectionFactory
                    .createConnection();
            SOAPMessage soapRequest = createSOAPRequest(request);
            //soapRequest.writeTo(System.out);
            
            if(log.isDebugEnabled()){
                StringWriter writer = new StringWriter();  
                
                StreamResult result = new StreamResult(writer);  
                TransformerFactory transformerFactory = TransformerFactory.newInstance();  
                Transformer transformer = transformerFactory.newTransformer();  
                transformer.transform(soapRequest.getSOAPPart().getContent(), result);  
                
                log.debug(writer.getBuffer());
            }
            soapResponse = soapConnection.call(soapRequest,url);

        } catch (UnsupportedOperationException | SOAPException | IOException | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
            try {            
                if(soapConnection != null) soapConnection.close();
            } catch (SOAPException ex) {
                //consume the exception
            }
        }
        return soapResponse;
    }
    
    private static SOAPMessage createSOAPRequest(String request) throws SOAPException, IOException {
        MessageFactory messageFactory = MessageFactory.newInstance();

        InputStream stream = new ByteArrayInputStream(request.getBytes(StandardCharsets.UTF_8));

        SOAPMessage soapMessage = messageFactory.createMessage(new MimeHeaders(), stream);
        soapMessage.getMimeHeaders().addHeader("Auth_user", "stub");
        
        return soapMessage;
    }

    
}

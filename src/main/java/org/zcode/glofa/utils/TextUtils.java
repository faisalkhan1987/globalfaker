/**
 * 
 */
package org.zcode.glofa.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

/**
 * @author fkhan
 *
 */
public class TextUtils {

	/**
	 * @param soapMessage
	 * @return
	 */
	public static String convertSoapToString(SOAPMessage soapMessage) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			soapMessage.writeTo(out);
		} catch (SOAPException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new String(out.toByteArray());
	}
}

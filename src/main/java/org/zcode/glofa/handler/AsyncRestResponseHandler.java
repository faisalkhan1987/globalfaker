/**
 * 
 */
package org.zcode.glofa.handler;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.zcode.glofa.domain.StubConfig;
import org.zcode.glofa.params.SOAPParamResolver;

/**
 * @author fkhan
 *
 */
@Component("asyncRestResponseHandler")
public class AsyncRestResponseHandler extends SyncRestResponseHandler{

	@Autowired
	SOAPParamResolver paramResolver;
	
	/* (non-Javadoc)
	 * @see org.zcode.glofa.handler.StubResponseHandler#fake(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.zcode.glofa.domain.StubConfig)
	 */
	@Override
	public void fakeResponse(HttpServletRequest request, HttpServletResponse response, StubConfig stubConfig) throws IOException, SOAPException {
		super.fakeResponse(request, response, stubConfig);
		response.getOutputStream().flush();
		executeAsyncActions(stubConfig);
	}

	@Async
	private void executeAsyncActions(StubConfig stubConfig) throws SOAPException{
		String asyncResponse = stubConfig.getAsyncResponse();
		
		if(stubConfig.getInputParams() != null && stubConfig.getInputParams().length > 0){
			for(String param : stubConfig.getInputParams()){
				if(this.paramValueMap.get(param) != null && this.paramValueMap.get(param).length > 0) {
					asyncResponse = asyncResponse.replace(param, this.paramValueMap.get(param)[0]);
				}
			}
		}
		
//		HttpUtils.triggerRequest(asyncResponse, stubConfig.getCallbackUrl());
	}
}

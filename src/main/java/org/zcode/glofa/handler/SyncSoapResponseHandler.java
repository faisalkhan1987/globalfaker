/**
 * 
 */
package org.zcode.glofa.handler;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zcode.glofa.builder.ContentBuilder;
import org.zcode.glofa.domain.StubConfig;
import org.zcode.glofa.logs.Logger;
import org.zcode.glofa.utils.SOAPUtils;
import org.zcode.glofa.utils.TextUtils;

/**
 * @author fkhan
 *
 */
@Component("syncSoapResponseHandler")
public class SyncSoapResponseHandler implements StubResponseHandler<SOAPMessage> {
	
	@Autowired
	protected ContentBuilder<String, SOAPMessage> soapResponseBuilder;

	@Autowired
	protected Logger soapLogger;

	protected SOAPMessage soapMessage;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.zcode.glofa.handler.StubResponseHandler#fake(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * org.zcode.glofa.domain.StubConfig)
	 */
	@Override
	public void fakeResponse(HttpServletRequest request, HttpServletResponse response, StubConfig stubConfig)
			throws IOException, SOAPException {

		soapMessage = SOAPUtils.createSoapMessageFromHttpRequest(request);

		String fakeResponse = soapResponseBuilder.buildContent(soapMessage, stubConfig.getSyncResponse(),
				Arrays.asList(stubConfig.getInputParams()));

		response.getOutputStream().write(fakeResponse.getBytes());

		String soapRequest = TextUtils.convertSoapToString(soapMessage);

		soapLogger.log(soapRequest, stubConfig);
	}

}

/**
 * 
 */
package org.zcode.glofa.handler;

import org.springframework.stereotype.Component;
import org.zcode.glofa.domain.StubConfig;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author fkhan
 *
 */
@Component
public interface StubResponseHandler<ContentType> {

	void fakeResponse(HttpServletRequest request, HttpServletResponse response, StubConfig stubConfig) throws Exception;
}

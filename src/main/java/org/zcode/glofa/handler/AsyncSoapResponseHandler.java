/**
 * 
 */
package org.zcode.glofa.handler;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.zcode.glofa.domain.StubConfig;
import org.zcode.glofa.utils.SOAPUtils;

/**
 * @author fkhan
 *
 */
@Component("asyncSoapResponseHandler")
public class AsyncSoapResponseHandler extends SyncSoapResponseHandler{

	@Autowired
	private SOAPUtils soapUtils;
	
	
	/* (non-Javadoc)
	 * @see org.zcode.glofa.handler.StubResponseHandler#fake(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.zcode.glofa.domain.StubConfig)
	 */
	@Override
	public void fakeResponse(HttpServletRequest request, HttpServletResponse response, StubConfig stubConfig) throws IOException, SOAPException {
		super.fakeResponse(request, response, stubConfig);
		response.getOutputStream().flush();
		executeAsyncActions(this.soapMessage, stubConfig);
	}

	@Async
	public void executeAsyncActions(SOAPMessage soapMessage, StubConfig stubConfig) throws SOAPException{
		
		String asyncResponse = soapResponseBuilder.buildContent(soapMessage, stubConfig.getAsyncResponse(),
				Arrays.asList(stubConfig.getInputParams()));

		soapUtils.triggerRequest(asyncResponse, stubConfig.getCallbackUrl());
	}
}

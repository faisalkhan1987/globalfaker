/**
 * 
 */
package org.zcode.glofa.handler;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zcode.glofa.domain.StubConfig;
import org.zcode.glofa.repo.StubLogRepo;

/**
 * @author fkhan
 *
 */
@Component("syncRestResponseHandler")
public class SyncRestResponseHandler implements StubResponseHandler<String>{

	@Autowired
	StubLogRepo stubLogRepo;
	
	Map<String, String[]> paramValueMap;
	/* (non-Javadoc)
	 * @see org.zcode.glofa.handler.StubResponseHandler#fake(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.zcode.glofa.domain.StubConfig)
	 */
	@Override
	public void fakeResponse(HttpServletRequest request, HttpServletResponse response, StubConfig stubConfig) throws IOException, SOAPException {
		this.paramValueMap = request.getParameterMap();
		String fakeResponse = stubConfig.getSyncResponse();

		if(stubConfig.getInputParams() != null){
			for(String inputParam : stubConfig.getInputParams()) {
				if(paramValueMap.get(inputParam) != null && paramValueMap.get(inputParam).length > 0) {
					fakeResponse = fakeResponse.replace(inputParam, paramValueMap.get(inputParam)[0]);
				}
			}
		}
		response.getOutputStream().write(fakeResponse.getBytes());
	}

}

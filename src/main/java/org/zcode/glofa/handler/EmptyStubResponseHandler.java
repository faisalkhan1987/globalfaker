/**
 * 
 */
package org.zcode.glofa.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.zcode.glofa.domain.StubConfig;

/**
 * @author fkhan
 *
 */
@Component("emptyStubResponseHandler")
public class EmptyStubResponseHandler implements StubResponseHandler<String>{

	/* (non-Javadoc)
	 * @see org.zcode.glofa.handler.StubResponseHandler#fake(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.zcode.glofa.domain.StubConfig)
	 */
	@Override
	public void fakeResponse(HttpServletRequest request, HttpServletResponse response, StubConfig stubConfig) {
		
	}

}

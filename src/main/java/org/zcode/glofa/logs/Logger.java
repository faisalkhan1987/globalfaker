package org.zcode.glofa.logs;

import org.zcode.glofa.domain.StubConfig;

/**
 * @author fkhan
 *
 * @param <ContentType>
 */
public interface Logger {
	
	/**
	 * @param contentType
	 */
	void log(String requestContent, StubConfig stubConfig);

}

package org.zcode.glofa.logs;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zcode.glofa.config.AppConstants;
import org.zcode.glofa.domain.StubConfig;
import org.zcode.glofa.domain.StubLog;
import org.zcode.glofa.repo.StubLogRepo;

/**
 * @author fkhan
 *
 */
@Component("soapLogger")
public class SOAPLogger implements Logger{

	@Autowired
	StubLogRepo stubLogRepo;


	@Override
	public void log(String requestContent, StubConfig stubConfig) {
		if (stubConfig.isLogEnabled()) {
			stubLogRepo.save(StubLog.builder().stubName(stubConfig.getStubName())
					.request(requestContent)
					.creationDate(new Date())
					.type(stubConfig.getProtocol())
					.build());
		}
	}
}

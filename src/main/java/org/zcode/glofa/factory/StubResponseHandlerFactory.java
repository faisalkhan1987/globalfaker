/**
 * 
 */
package org.zcode.glofa.factory;

import javax.xml.soap.SOAPMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zcode.glofa.config.AppConstants;
import org.zcode.glofa.handler.StubResponseHandler;

/**
 * @author fkhan
 *
 */
@Component
public final class StubResponseHandlerFactory {
	
	@Autowired
	StubResponseHandler<SOAPMessage> syncSoapResponseHandler;
	
	@Autowired
	StubResponseHandler<SOAPMessage> asyncSoapResponseHandler;

	@Autowired
	StubResponseHandler<String> syncRestResponseHandler;
	
	@Autowired
	StubResponseHandler<String> asyncRestResponseHandler;
	
	@Autowired
	StubResponseHandler<String> emptyStubResponseHandler;
	
	
	public StubResponseHandler getHandler(String stubMode, String protocol){
		switch(protocol) {
		case AppConstants.REST:
			switch(stubMode) {
			case AppConstants.SYNC:
				return syncRestResponseHandler;
			case AppConstants.ASYNC:
				return asyncRestResponseHandler;
			}
			break;
		case AppConstants.SOAP:
			switch(stubMode) {
			case AppConstants.SYNC:
				return syncSoapResponseHandler;
			case AppConstants.ASYNC:
				return asyncSoapResponseHandler;
			}
			break;
		default:
			return emptyStubResponseHandler;
		}
		return emptyStubResponseHandler;
	}

}

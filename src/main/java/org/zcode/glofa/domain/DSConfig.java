package org.zcode.glofa.domain;

import java.sql.Timestamp;

import org.springframework.data.mongodb.core.index.Indexed;

public class DSConfig {

    @Indexed(unique = true)
    private String dsName;

    private String contextPath;

    private String driverClassName;

    private String userName;

    private String encryptedPassword;

    private String jdbcConnectionString;

    private Timestamp creationTime;

    private String creationUser;

    private Timestamp modificationTime;

    private String modificationUser;


    /**
     * @return the dsName
     */
    public String getDsName() {
	return dsName;
    }

    /**
     * @param dsName
     *            the dsName to set
     */
    public void setDsName(String dsName) {
	this.dsName = dsName;
    }

    /**
     * @return the contextPath
     */
    public String getContextPath() {
	return contextPath;
    }

    /**
     * @param contextPath
     *            the contextPath to set
     */
    public void setContextPath(String contextPath) {
	this.contextPath = contextPath;
    }

    /**
     * @return the driverClassName
     */
    public String getDriverClassName() {
	return driverClassName;
    }

    /**
     * @param driverClassName
     *            the driverClassName to set
     */
    public void setDriverClassName(String driverClassName) {
	this.driverClassName = driverClassName;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
	return userName;
    }

    /**
     * @param userName
     *            the userName to set
     */
    public void setUserName(String userName) {
	this.userName = userName;
    }

    /**
     * @return the encryptedPassword
     */
    public String getEncryptedPassword() {
	return encryptedPassword;
    }

    /**
     * @param encryptedPassword
     *            the encryptedPassword to set
     */
    public void setEncryptedPassword(String encryptedPassword) {
	this.encryptedPassword = encryptedPassword;
    }

    /**
     * @return the creationTime
     */
    public Timestamp getCreationTime() {
	return creationTime;
    }

    /**
     * @param creationTime
     *            the creationTime to set
     */
    public void setCreationTime(Timestamp creationTime) {
	this.creationTime = creationTime;
    }

    /**
     * @return the creationUser
     */
    public String getCreationUser() {
	return creationUser;
    }

    /**
     * @param creationUser
     *            the creationUser to set
     */
    public void setCreationUser(String creationUser) {
	this.creationUser = creationUser;
    }

    /**
     * @return the modificationTime
     */
    public Timestamp getModificationTime() {
	return modificationTime;
    }

    /**
     * @param modificationTime
     *            the modificationTime to set
     */
    public void setModificationTime(Timestamp modificationTime) {
	this.modificationTime = modificationTime;
    }

    /**
     * @return the modificationUser
     */
    public String getModificationUser() {
	return modificationUser;
    }

    /**
     * @param modificationUser
     *            the modificationUser to set
     */
    public void setModificationUser(String modificationUser) {
	this.modificationUser = modificationUser;
    }

    /**
     * @return the jdbcConnectionString
     */
    public String getJdbcConnectionString() {
        return jdbcConnectionString;
    }

    /**
     * @param jdbcConnectionString the jdbcConnectionString to set
     */
    public void setJdbcConnectionString(String jdbcConnectionString) {
        this.jdbcConnectionString = jdbcConnectionString;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "DSConfig [dsName=" + dsName
		+ ", contextPath=" + contextPath + ", driverClassName="
		+ driverClassName + ", userName=" + userName
		+ ", encryptedPassword=" + encryptedPassword
		+ ", jdbcConnectionString=" + jdbcConnectionString + "]";
    }

}

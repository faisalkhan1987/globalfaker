package org.zcode.glofa.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.annotation.Id;

/**
 * @author fkhan
 *
 */
public class Task implements Serializable{

    @Id
    protected String taskId;
    
    protected String name;

    protected String description;

    protected Timestamp creationTime;

    protected String creationUser;

    protected Timestamp modificationTime;

    protected String modificationUser;

    protected List<String> inputParams;
    
    protected String outParam;
    
    protected String executionUser;
    
    protected long sleepTime;
    
    public Task(){
    	this.sleepTime = 100;
    }

	/**
	 * @return the executionUser
	 */
	public String getExecutionUser() {
		return executionUser;
	}

	/**
	 * @param executionUser the executionUser to set
	 */
	public void setExecutionUser(String executionUser) {
		this.executionUser = executionUser;
	}

	/**
     * @return the creationTime
     */
    public Timestamp getCreationTime() {
        return creationTime;
    }

    /**
     * @param creationTime the creationTime to set
     */
    public void setCreationTime(Timestamp creationTime) {
        this.creationTime = creationTime;
    }

    /**
     * @return the creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * @param creationUser the creationUser to set
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * @return the modificationTime
     */
    public Timestamp getModificationTime() {
        return modificationTime;
    }

    /**
     * @param modificationTime the modificationTime to set
     */
    public void setModificationTime(Timestamp modificationTime) {
        this.modificationTime = modificationTime;
    }

    /**
     * @return the modificationUser
     */
    public String getModificationUser() {
        return modificationUser;
    }

    /**
     * @param modificationUser the modificationUser to set
     */
    public void setModificationUser(String modificationUser) {
        this.modificationUser = modificationUser;
    }


    /**
     * @return the inputParams
     */
    public List<String> getInputParams() {
        return inputParams;
    }

    /**
     * @param inputParams the inputParams to set
     */
    public void setInputParams(List<String> inputParams) {
        this.inputParams = inputParams;
    }

    /**
     * @return the outParam
     */
    public String getOutParam() {
        return outParam;
    }

    /**
     * @param outParam the outParam to set
     */
    public void setOutParam(String outParam) {
        this.outParam = outParam;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the taskId
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * @param taskId the taskId to set
     */
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    
    /**
	 * @return the sleepTime
	 */
	public long getSleepTime() {
		return sleepTime;
	}

	/**
	 * @param sleepTime the sleepTime to set
	 */
	public void setSleepTime(long sleepTime) {
		this.sleepTime = sleepTime;
	}

	/* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "Task [taskId=" + taskId + ", name=" + name 
		+ "]";
    }

    
    
    
    
}

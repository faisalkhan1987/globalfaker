/**
 *
 */
package org.zcode.glofa.domain;

import java.util.Date;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * @author fkhan
 *
 */
@Data
@Builder
public class StubLog {

    @Indexed
    private String stubName;

    private String request;

    @DateTimeFormat
    private Date creationDate;

    private String type;

}

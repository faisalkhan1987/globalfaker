/**
 *
 */
package org.zcode.glofa.domain;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.List;

/**
 * @author fkhan
 *
 */
@Data
@Builder
public class StubConfig {

    @Id
    protected String stubId;

    @Indexed(unique = true)
    private String stubName;

    private String stubUrl = "";

    private String stubMode;// Sync or Async

    private int responseDelay;

    private String protocol;

    private String soapAction;

    private String syncResponse;

    private String asyncResponse;

    private String callbackUrl;

    private String[] inputParams;

    private String validationXsdFileName;

    private List<Task> dynamicTasks;

    private boolean logEnabled;

}

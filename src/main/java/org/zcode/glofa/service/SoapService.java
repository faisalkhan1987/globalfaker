package org.zcode.glofa.service;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zcode.glofa.config.AppConstants;
import org.zcode.glofa.domain.StubConfig;
import org.zcode.glofa.factory.StubResponseHandlerFactory;
import org.zcode.glofa.handler.StubResponseHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Log4j2
@Component
public class SoapService {

    @Autowired
    private StubsConfigService stubsConfigService;

    @Autowired
    private StubResponseHandlerFactory stubResponseHandlerFactory;

    @SneakyThrows
    public void serviceRequest(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType(request.getContentType());

        String contextPath = request.getContextPath();
        String stubUrl = request.getRequestURI().replace(contextPath, "");// to extract the stub url
        String soapAction = request.getHeader("soapaction");

        StubConfig stubConfig = stubsConfigService.getStubConfig(soapAction, stubUrl);
        StubResponseHandler stubHandler = stubResponseHandlerFactory.getHandler(stubConfig.getStubMode(), AppConstants.SOAP);
        stubHandler.fakeResponse(request, response, stubConfig);

    }
}

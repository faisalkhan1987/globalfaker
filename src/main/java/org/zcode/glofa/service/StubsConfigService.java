package org.zcode.glofa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zcode.glofa.config.AppConstants;
import org.zcode.glofa.domain.StubConfig;
import org.zcode.glofa.repo.StubConfigRepo;

@Component
public class StubsConfigService {

	@Autowired
	private StubConfigRepo stubConfigRepo;

	public StubConfig getStubConfig(String soapAction, String stubUrl){
		StubConfig stubConfig = null;
		if(soapAction != null){
			soapAction = soapAction.replace("\"", ""); //to replace " character in the soap action
			stubConfig = stubConfigRepo.findBySoapActionAndStubUrl(soapAction, stubUrl);
		}
		if(stubConfig == null){
			stubConfig = stubConfigRepo.findByStubUrl(stubUrl);
		}
		return stubConfig;
	}

	public StubConfig findStubByStubUrl(String stubUrl) {
		StubConfig stubConfig = stubConfigRepo.findByStubUrl(stubUrl);
		
		if(stubConfig == null) {
			List<StubConfig> stubConfigList = stubConfigRepo.findAll();

			for(StubConfig stub : stubConfigList) {
				String regexUrl = getRegexUrl(stub);
				if(stubUrl.matches(regexUrl)) {
					stubConfig = stub;
					break;
				}
			}
			
		}
		return stubConfig;
	}

	private String getRegexUrl(StubConfig stubConfig) {
		String stubUrl = stubConfig.getStubUrl();
		String[] inputParams = stubConfig.getInputParams();
		if(inputParams != null && inputParams.length > 0) {
			for(String inputParam : stubConfig.getInputParams()) {
				stubUrl = stubUrl.replace(inputParam, AppConstants.REGEX_STRING);
			}
		}
		return stubUrl;
	}
}

package org.zcode.glofa.service;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zcode.glofa.config.AppConstants;
import org.zcode.glofa.domain.StubConfig;
import org.zcode.glofa.factory.StubResponseHandlerFactory;
import org.zcode.glofa.handler.StubResponseHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Random;

@Component
public class RestService {

    private Random random = new Random();
    @Autowired
    private StubsConfigService stubsConfigService;

    @Autowired
    private StubResponseHandlerFactory stubResponseHandlerFactory;

    @SneakyThrows
    public void serviceRequest(HttpServletRequest request, HttpServletResponse response) {
        String contextPath = request.getContextPath();
        String stubUrl = request.getRequestURI().replace(contextPath, "") ;
        if(request.getQueryString() != null && !AppConstants.EMPTY_STRING.equals(request.getQueryString())){
            stubUrl += "?" + request.getQueryString();// to extract the stub url
        }

        StubConfig stubConfig = stubsConfigService.findStubByStubUrl(stubUrl);
        Thread.sleep(stubConfig.getResponseDelay());

        StubResponseHandler stubHandler = stubResponseHandlerFactory.getHandler(stubConfig.getStubMode(), AppConstants.REST);
        stubHandler.fakeResponse(request, response, stubConfig);
    }
}

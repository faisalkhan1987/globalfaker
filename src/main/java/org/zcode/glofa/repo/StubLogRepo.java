/**
 * 
 */
package org.zcode.glofa.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.zcode.glofa.domain.StubLog;

/**
 * @author fkhan
 *
 */
public interface StubLogRepo extends MongoRepository<StubLog, String>{

	List<StubLog> findByStubName(String stubName);
}

/**
 * 
 */
package org.zcode.glofa.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.zcode.glofa.domain.StubConfig;

/**
 * @author fkhan
 *
 */
public interface StubConfigRepo extends MongoRepository<StubConfig, String>{

	StubConfig findBySoapActionAndStubUrl(String soapAction, String stubUrl);
	
	StubConfig findByStubUrl(String stubUrl);
	
	StubConfig findByStubUrlLike(String stubUrl);
}

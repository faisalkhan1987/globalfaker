/**
 * 
 */
package org.zcode.glofa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.zcode.glofa.domain.StubConfig;
import org.zcode.glofa.domain.StubLog;
import org.zcode.glofa.repo.StubConfigRepo;
import org.zcode.glofa.repo.StubLogRepo;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author fkhan
 *
 */
@RequestMapping("/admin")
@RestController
public class HomePageController {

	@Autowired
	StubConfigRepo stubConfigRepo;
	
	@Autowired
	StubLogRepo stubLogRepo;
	
	@RequestMapping(value = "/getAllStubs", method = GET)
	public List<StubConfig> getAllStubs(){
		return stubConfigRepo.findAll();
	}
	
	@RequestMapping(value = "/getStubLogsByName", method = GET)
	public List<StubLog> getStubLogsById(@RequestParam("stubName") String stubName){
		return stubLogRepo.findByStubName(stubName);
	}

}

/**
 * 
 */
package org.zcode.glofa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zcode.glofa.domain.StubConfig;
import org.zcode.glofa.repo.StubConfigRepo;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author fkhan
 *
 */
@RequestMapping("/admin")
@RestController
public class AdminController {

	@Autowired
	StubConfigRepo stubConfigRepo;
	
	@RequestMapping(value = "/save", method = POST)
	public void saveStubConfig(@RequestBody StubConfig stubConfig) {
		stubConfigRepo.save(stubConfig);
	}
	
}

package org.zcode.glofa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zcode.glofa.service.RestService;
import org.zcode.glofa.service.SoapService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class VirtualApiController {

    @Autowired
    private SoapService soapService;

    @Autowired
    private RestService restService;

    @RequestMapping("/soap/**")
    public void doSoapCall(HttpServletRequest request, HttpServletResponse response) {
        soapService.serviceRequest(request, response);
    }

    @RequestMapping("/rest/**")
    public void doRestCall(HttpServletRequest request, HttpServletResponse response) {
        restService.serviceRequest(request, response);
    }
}

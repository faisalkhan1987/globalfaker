package org.zcode.glofa.params;

import java.util.List;
import java.util.Map;

/**
 * @author fkhan
 *
 */
public interface ParamResolver<InputContentType> {

	/**
	 * @param params
	 * @param contentType
	 * @return
	 */
	Map<String,String> resolveParams(List<String> params, InputContentType contentType);
	
}

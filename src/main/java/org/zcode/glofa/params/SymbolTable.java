/**
 * 
 */
package org.zcode.glofa.params;

import java.util.HashMap;
import java.util.Map;

/**
 * @author fkhan
 *
 */
public class SymbolTable {

    private final ThreadLocal thLocal = new ThreadLocal();
    
    private static SymbolTable singletonInstance = null;
    
    private SymbolTable(){}
    
    /**
     * @return
     */
    public static final SymbolTable getInstance(){
		if(singletonInstance == null){
		    singletonInstance = new SymbolTable();
		}
		return singletonInstance;
    }
    
    public final void init(){
		Map valueMap = new HashMap();
		thLocal.set(valueMap);
    }
    
    public final void close(){
		Map valueMap = (Map)thLocal.get();
		valueMap.clear();
		valueMap = null;
    }
    
    public void put(String key, Object value){
    	((Map)thLocal.get()).put(key, value);
    }
    
    public void putAll(Map map){
    	if(map != null)
    		((Map)thLocal.get()).putAll(map);
    }
    
    public Map getAll(){
    	return (Map)thLocal.get();
    }
    
    public Object get(String key){
    	return ((Map)thLocal.get()).get(key);
    }
    
    
}

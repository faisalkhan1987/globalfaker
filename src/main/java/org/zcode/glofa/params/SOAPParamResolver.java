package org.zcode.glofa.params;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.springframework.stereotype.Component;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author fkhan
 *
 */
@Component("soapParamResolver")
public class SOAPParamResolver implements ParamResolver<SOAPMessage>{

	Map<String, String> valueMap;
	
	public SOAPParamResolver(){
		valueMap = new HashMap<String,String>();
	}
	/**
	 * @param params
	 * @param soapMessage
	 * @return
	 * @throws SOAPException
	 */
	public Map<String,String> resolveParams(List<String> params, SOAPMessage soapMessage){
		
		SOAPBody soapBody;
		try {
			soapBody = soapMessage.getSOAPBody();
			visit("",soapBody.getChildNodes(), params);
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return valueMap;
	}
	
	/**
	 * @param nodeName
	 * @param nodeList
	 * @param params
	 */
	public void visit(String nodeName, NodeList nodeList, List<String> params){
		for(int i = 0;i < nodeList.getLength();i++){
			Node node = nodeList.item(i);
			String localName = nodeName;
			if(node.getLocalName() != null){
				localName = localName + "."+node.getLocalName();
			}
			if(localName != null && localName.length() > 1){
				String varName = localName.substring(1);
				if(params.contains(varName)){
					if(node.getNodeValue() != null){
						valueMap.put(varName, node.getNodeValue().trim());
					}
				}
			}
			if(node.hasChildNodes()){
				visit(localName, node.getChildNodes(), params);
			}
		}
	}
}
